
#ifndef CLIST
#define CLIST

struct clientNode {
    char *IP;
    int start;
    char *secondsOnline;
    struct clientNode* next;
};

bool isMember(char *IP);
void removeClient(char *IP);
void addClient(char* IP, int start);
void writeList();

#endif
