
#include <stdio.h>

void writeLog(int actionCode, char *message, char *agentIP) {
        //char lBuffer[MAXBUF];
        memset(lBuffer, 0, MAXBUF);
        switch(actionCode) {
           case 0:
                //no response
                sprintf(lBuffer, "%s: No response is supplied to non-member agent %s\n", getTimeForLog(), agentIP);
                fputs(lBuffer, logFile);
           case 1:
              //recieved
                fprintf(logFile, "%s: Recieved a %s action from agent %s\n", getTimeForLog(), message, agentIP);
           case 2:
              //responded
                sprintf(lBuffer, "%s: Responded to agent %s with %s\n", getTimeForLog(), agentIP, message);
                fputs(lBuffer, logFile);
           default:
                break;
        }
}
