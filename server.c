
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/tcp.h>

#define MAXBUF 1024
#define MAXPORT 6
#define BACKLOG 5


// for linked list of active agents
struct clientNode {
    char *IP;
    int start; // capture start time as seconds past epoch
    struct clientNode* next;
};

struct clientNode *head = NULL;
struct clientNode *current = NULL;

FILE *logFile;

void getTimeForLog(char *tBuffer);
void writeLog(int actionCode, char *message, char *agentIP);
void addClient(char *cIP, int cStart);
bool isMember(char *cIP);
void removeClient(char *cIP);

int main(int argc, char *argv[]) {

  char cPort[MAXPORT];
  int PORT;

  if (argc < 2)
    {
      printf ("Usage: server served_on_port \r\n");
      return(0);
    }

  memset(cPort, 0, MAXPORT);
  sprintf(cPort,"%s",argv[1]);
  PORT = atoi(cPort);


    int server_fd, new_socket;
    int opt = 1;

    /* ----FOR REFERENCE----
    struct sockaddr_in {
    	short            sin_family;   // e.g. AF_INET
    	unsigned short   sin_port;     // e.g. htons()
    	struct in_addr   sin_addr;     // see struct in_addr
    	char             sin_zero[8];  // zero
    };

    struct in_addr {
    	unsigned long s_addr;  // load with inet_aton()
    };
    */

    struct sockaddr_in client_address;
    struct sockaddr_in server_address;
    int addrlen = sizeof(server_address);

    // SOCKET
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // avoid address already in use error
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt)) < 0)                                                                                                       {                                                                                                                                                                           perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons( PORT );

    // BIND
    if (bind(server_fd, (struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    // LISTEN
    if (listen(server_fd, BACKLOG) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    // open text file for log
    logFile = fopen("log.txt", "w+");
    if(logFile == NULL) {
	perror("File failed");
	exit(EXIT_FAILURE);
    }

    while(1) {
	// ACCEPT
    	if ((new_socket = accept(server_fd, (struct sockaddr *)&client_address,(socklen_t*)&addrlen)) < 0) {
       	    perror("accept");
            exit(EXIT_FAILURE);
    	}

	//inet_ntoa returns pointer to string
	char *clientIP;
	clientIP = inet_ntoa(client_address.sin_addr);

    	char rBuffer[MAXBUF];
	char wBuffer[MAXBUF];

	int actionCode = 0;
	/*-------used for writeLog()--------
	0 - No response provided
	1 - Received message from client
	2 - Responde to message
	3 - Requested item sent to client
	*/
    	int bytes_read = 0;

    	do {
            memset(rBuffer,0,MAXBUF);
	    memset(wBuffer,0, MAXBUF);

	    bytes_read = read(new_socket, rBuffer, MAXBUF);

	    if(strcmp(rBuffer, "JOIN") == 0) {
		//writeLog(actionCode, message, client)
		// In this case actionCode = 1 = recieved msg
		writeLog(1, rBuffer, clientIP);

		// does client IP exist in linked list ie..is a member
		if(isMember(clientIP)) {
		    sprintf(wBuffer, "ALREADY MEMBER");
		    write(new_socket, wBuffer, strlen(wBuffer));

		} else {
		   //add client IP and start time to linked list
		   addClient(clientIP, time(NULL));
		   sprintf(wBuffer, "OK");
		   write(new_socket, wBuffer, strlen(wBuffer));
		}
		// note in log that response was sent
		writeLog(2, wBuffer, clientIP);

	    } else if (strcmp(rBuffer, "LIST") == 0) {

		writeLog(1, rBuffer, clientIP);

		if(isMember(clientIP)) {
		    struct clientNode *ptr = head;
		    while(ptr != NULL) {
			sprintf(wBuffer, "%s , %li", ptr->IP, (time(NULL) - ptr->start));
       	        	write(new_socket, wBuffer, strlen(wBuffer));
			ptr = ptr->next;
		    }
		    actionCode = 3;
		} else {
		    // note in log that no response was give to clientIP
		    actionCode = 0;
		}

		writeLog(actionCode, rBuffer, clientIP);

	    } else if (strcmp(rBuffer, "LOG") == 0) {

		writeLog(1, rBuffer, clientIP);
		memset(wBuffer, 0, MAXBUF);

		if(isMember(clientIP)) {
		    //read from beginning of log file
		    rewind(logFile);

		    while(fgets(wBuffer, MAXBUF, logFile) != NULL) {
			write(new_socket, wBuffer, strlen(wBuffer));
        		memset(wBuffer, 0, MAXBUF);
		    }

		   actionCode = 3;

		} else { actionCode = 0;}

		writeLog(actionCode, rBuffer, clientIP);

	    } else if (strcmp(rBuffer, "LEAVE") == 0) {

		writeLog(1, rBuffer, clientIP);

		if(isMember(clientIP)) {
		   removeClient(clientIP);
	           sprintf(wBuffer, "OK");
                   write(new_socket, wBuffer, strlen(wBuffer));

		} else {
		  sprintf(wBuffer, "NOT MEMBER");
		  write(new_socket, wBuffer, strlen(wBuffer));
		}

		writeLog(2, wBuffer, clientIP);

	   } else {
		  writeLog(1, rBuffer, clientIP);
		  sprintf(wBuffer, "INVALID REQUEST");
		  write(new_socket, wBuffer, strlen(wBuffer));
		  writeLog(2, wBuffer, clientIP);
	     }

        } while ( bytes_read > 0 );
    }

    close(new_socket);
    fclose(logFile);

    return 0;
}

void getTimeForLog(char *tBuffer) {

    struct timespec curTime;
    char buffer [64];
    long ms; // Milliseconds
    time_t s;  // Seconds

    memset(tBuffer, 0, MAXBUF);

    clock_gettime(CLOCK_REALTIME, &curTime);

    s  = curTime.tv_sec;
    ms = (curTime.tv_nsec / 1.0e6); // nanoseconds to milliseconds
    if (ms > 999) {
        s++;                                                                                                                                                                                                 ms = 0;
    }

    strftime(buffer, sizeof buffer, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));

    //print from buffer to tBuffer
    sprintf(tBuffer, "%s:%03ld", buffer, ms);
}

void writeLog(int ac, char *message, char *agentIP) {
	char tBuffer[MAXBUF];
        getTimeForLog(tBuffer);

	switch(ac) {
	   case 0:
		//no response
	   	fprintf(logFile, "%s: No response is supplied to agent %s\n", tBuffer, agentIP);
		break;

	   case 1:
	      //recieved
	   	fprintf(logFile, "%s: Recieved a %s action from agent %s\n", tBuffer, message, agentIP);
		break;

	   case 2:
	      //responded
	   	fprintf(logFile, "%s: Responded to agent %s with %s\n", tBuffer, agentIP, message);
		break;

	   case 3:
	      //item sent
		fprintf(logFile, "%s: %s sent to agent %s\n", tBuffer, message, agentIP);
		break;

   	   default:
		break;
	}

	fflush(logFile);
}

void addClient(char *cIP, int cStart) {
   printf("%s\n", cIP); // DELETE ME
   //create node
   struct clientNode *newClient;
   //malloc = memory allocation, returns pointer
   newClient = (struct clientNode *) malloc(sizeof(struct clientNode));

   newClient->IP = cIP;
   newClient->start = cStart;
   newClient->next = head;
   //point to new first node
   head = newClient;
}

//is client with given IP an active member
bool isMember(char *cIP) {

   //start from the first link
   struct clientNode* current = head;

   //if list is empty
   if(head == NULL) {
      return false;
   }

   //navigate through list
   while(current->IP != cIP) {

      //if it is last node
      if(current->next == NULL) {
         return false;
      } else {
         current = current->next;
      }
   }

   return true;
}

//delete a client with given IP
void removeClient(char *cIP) {

   //start from the first link
   struct clientNode* current = head;
   struct clientNode* previous = NULL;

   //if list is empty
   if(head == NULL) {
      return;
   }

   //navigate through list
   while(current->IP != cIP) {

      //if it is last node
      if(current->next == NULL) {
         return;
      } else {
         //store reference to current link
         previous = current;
         current = current->next;
      }
   }

   //update the list
   if(current == head) {
      head = head->next;
   } else {
      previous->next = current->next;
   }
}
