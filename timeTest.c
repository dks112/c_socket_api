#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
//#include <timekeeping.h>
#include <stdbool.h>

#define MAXBUF 128

int main() {
  struct timeval tv;
  time_t s;
  long ms;
  struct tm *info;
  char buffer[64];
  char tBuffer[96];

  gettimeofday(&tv, NULL);
  s = tv.tv_sec;
  ms = (tv.tv_usec / 1000);
  if (ms > 999) {
    s++;
  }
  info = localtime(&s);

  strftime(buffer, sizeof buffer, "%Y-%m-%d %H:%M:%S", info);
  sprintf(tBuffer, "%s:%03ld", buffer, ms);
  printf("%s", tBuffer);
  return 0;
}

