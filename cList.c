#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

/* --- FOR REFERENCE ---
struct clientNode {
    char *IP;
    int start;
    char *secondsOnline;
    struct clientNode* next;
};
*/
struct clientNode *head = NULL;
struct clientNode *current = NULL;

//display the list
struct clientNode* getList() {

   struct clientNode *ptr = head;
   int intActive;
   //char strActive[] = " ";
   //start from the beginning
   while(ptr != NULL) {
      intActive =  ptr->start;
      intActive = time(NULL) - intActive; //don't need time active as part of struct
      sprintf(secondsOnline, "%d", intActive);
      ptr = ptr->next;
      return node pointer;
   }
}

//insert link at the front
void addClient(char *cIP, int cStart) {

   //create node
   struct clientNode *newClient;
   //malloc = memory allocation, returns pointer
   newClient = (struct clientNode *) malloc(sizeof(struct clientNode));

   newClient->IP = cIP;
   newClient->start = cStart;
   newClient->next = head;
   //point to new first node
   head = newClient;
}

//find a client with given IP
bool isMember(char *cIP) {

   //start from the first link
   struct clientNode* current = head;

   //if list is empty
   if(head == NULL) {
      return false;
   }

   //navigate through list
   while(current->IP != cIP) { // might need strcmp();

      //if it is last node
      if(current->next == NULL) {
         return false;
      } else {
         //go to next link
         current = current->next;
      }
   }

   //if client found, return the pointer
   return true;
}

//delete a link with given key
void removeClient(char *cIP) {

   //start from the first link
   struct clientNode* current = head;
   struct clientNode* previous = NULL;

   //if list is empty
   if(head == NULL) {
      return;
   }

   //navigate through list
   while(current->IP != cIP) {

      //if it is last node
      if(current->next == NULL) {
         return;
      } else {
         //store reference to current link
         previous = current;
         //move to next link
         current = current->next;
      }
   }

   //found a match, update the link
   if(current == head) {
      //change first to point to next link
      head = head->next;
   } else {
      //bypass the current link
      previous->next = current->next;
   }
}

