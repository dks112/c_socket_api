// This is the agent application that will be used to send actions to the server.

// It takes 3 arguments, server IP or host, port, and the action requested.

// List of includes

//gcc agent.c -o agent
//./agent

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>


//preprocessor definitions
#define MAXBUF 1024
#define MAXHOSTNAME 200
#define MAXACTION  10
#define MAXPORT 6


// argc (argument count) and argv (argument vector)
// Running it with ./test a1 b2 c3
int main (int argc,char* argv[]) {

  char cServer[MAXHOSTNAME];
  char cPort[MAXPORT];
  char cAction [MAXACTION];
  int nPort;

  if (argc < 4)
    {
      printf ("Usage: agent server_name server_port action_requested \r\n");
      return(0);
    }

  // Make sure its empty
  // Sets first argument to zero..
  memset(cServer,0,MAXHOSTNAME);
  sprintf(cServer,"%s",argv[1]);

  memset(cPort, 0, MAXPORT);
  sprintf(cPort,"%s",argv[2]);
  nPort = atoi(cPort);

  memset(cAction,0,MAXACTION);
  sprintf(cAction,"%s",argv[3]);


  /* (IPv4 only--see struct sockaddr_in6 for IPv6)

    struct sockaddr_in {
        short int          sin_family;  // Address family, AF_INET
        unsigned short int sin_port;    // Port number
        struct in_addr     sin_addr;    // Internet address
        unsigned char      sin_zero[8]; // Same size as struct sockaddr
    };
*/
  // Defines struct named "name"
  struct sockaddr_in name;
  struct hostent *hent;

  int sd;

    //--------------------SOCKET--------------------
    // CREATE a new socket to use for communication
    // if returned file descriptor is not -1
    // int sockfd = socket(DOMAIN, TYPE, POROTOCOL)

  if ((sd = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf (stderr, "ERROR: socket() failed\n");
    exit (-1);
  }
  fprintf(stdout,"Socket Created\n");


  // Used to correctly fill in the server host
  // gethosbyname() is equalivent but outdated to getaddinfo()
  if ((hent = gethostbyname (cServer)) == NULL) {
      fprintf (stderr, "ERROR: Host not found\n");
      exit(-1);
    }
  else
  // this might be happening here...?
  // load that information by hand into a struct sockaddr_in, and use that in your calls
  // &name.sin_addr = previously assigned struct
  // bcopy function copys bytes from s1 to s2
    bcopy (hent->h_addr, &name.sin_addr, hent->h_length);
  //  printf("hent->h_addr " + hent->h_addr);
  //  printf("&name.sin_addr " + &name.sin_addr);
  //  printf("hent->h_length " + hent->h_length);

  name.sin_family = AF_INET;

  // notice the host to network conversion.
  // Little to Big Endian
  // the sin_port must be in Network Byte Order (by using htons()
  name.sin_port = htons (nPort);


  //--------------connect to the server-------------------------
  // int connect(int sockfd, struct sockaddr *addr, socklen_t addrlen)

  if (connect (sd, (struct sockaddr *)&name, sizeof(name)) < 0) {
    fprintf (stderr, "ERROR: connect() failed\n");
    exit (-1);
  }
  printf("Connect OK\n");


  //--------------- send the action to the server------------------------
  int byteswritten = write (sd, cAction, strlen(cAction));

  printf("wrote %d\n",byteswritten);

  char buffer[MAXBUF];
  int bytes_read = 0;
  int total_bytes_read = 0;

 do {
    memset(buffer,0,MAXBUF);
    bytes_read = read(sd, buffer, MAXBUF);// ---------------- READ -----------------
    if (bytes_read < 0)
       break;
    fprintf(stdout,"Read %d bytes: [%s]\r\n", bytes_read,buffer);
    total_bytes_read += bytes_read;
  }
  while ( bytes_read > 0 );


  printf("Total Bytes Read = %d\r\n",total_bytes_read);

  close(sd);

  return 0;
}
